module GELF
  class Logger
    @level : ::Logger::Severity
    @disconnect_callback : Proc(GELF::Logger, Exception, Void)
    @buffer : GELF::Buffer
    @receive_channel = Channel(Slice(UInt8)).new
    @connection : GELF::Sender(TCPSocket)? | GELF::Sender(UDPSocket)?
    alias AnyValue = Int::Signed|Int::Unsigned|Nil|String|Float32|Float64|Bool
    property! host, port, protocol, facility, hostname, level, buffer_size
    property! connection_timeout, disconnect_callback, disconnect_count
    property connection
    getter! buffer, receive_channel

    def self.configure
      instance = new
      yield instance
      instance.connect
      instance
    end

    def initialize
      @host = "localhost"
      @port = 12001
      @protocol = "tcp"
      @facility = "local-app"
      @hostname = "local"
      @chunk_size = "small"
      @buffer_size = 1000
      @buffer = GELF::Buffer.new
      @level = ::Logger::INFO
      @disconnect_count = 0
      @connection_timeout = 5
      @disconnect_callback = -> (logger : GELF::Logger, error : Exception) { raise error }
    end

    def connect
      @connection ||= begin
        buffer.max_size = @buffer_size
        klass = @protocol == "tcp" ?  GELF::Sender(TCPSocket) : GELF::Sender(UDPSocket)
        klass.new(@host, @port, logger: self)
      end
    end

    def disconnect
      connection.close if @connection
      @disconnect_count += 1
      @connection = nil
    end

    def connection
      @connection.not_nil!
    end

    def max_chunk_size
      case @chunk_size
      when "big"
        8154
      else
        1420
      end
    end

    {% for level in ["DEBUG", "INFO", "WARN", "ERROR", "FATAL", "UNKNOWN"] %}
      def {{level.id.downcase}}(message : Hash(String, AnyValue|Hash(String, AnyValue)), progname : String? = nil)
        add(::Logger::{{level.id}}, message, progname)
      end

      def {{level.id.downcase}}(message : String, progname : String? = nil)
        add(::Logger::{{level.id}}, message, progname)
      end

      def {{level.id.downcase}}?
        ::Logger::{{level.id}} >= level
      end
    {% end %}

    private def add(level, message : Hash(String, AnyValue|Hash(String, AnyValue)), progname : String? = nil)
      message["_facility"] = progname || facility
      notify_with_level(level, message)
    end

    private def add(level, message : String, progname : String? = nil)
      hash = {} of String => AnyValue|Hash(String, AnyValue)
      hash["short_message"] = message
      add(level, hash, progname)
    end

    private def notify_with_level(level, message : Hash(String, AnyValue|Hash(String, AnyValue)))
      return if level < @level

      hash = {} of String => AnyValue|Hash(String, AnyValue)
      message.each do |k, v|
        if v.is_a?(Hash)
          hash[k] = {} of String => AnyValue
          v.each {|key, value| hash[k].as(Hash(String, AnyValue))[key] = value }
        else
          hash[k] = v
        end
      end

      hash["version"] = "1.1"
      hash["host"] = hostname
      hash["level"] = GELF::LOGGER_MAPPING[level]
      hash["timestamp"] = "%f" % Time.utc.to_unix_f
      hash["short_message"] ||= "Message must be set!"

      data = serialize_message(hash)
      
      if @protocol == "tcp" || data.size <= max_chunk_size
        @receive_channel.send(data)
      else
        msg_id = Random::Secure.hex(4)
        num_slices = (data.size / max_chunk_size.to_f).ceil.to_i

        num_slices.times do |index|
          io = IO::Memory.new

          # Magic bytes
          io.write_byte(0x1e_u8)
          io.write_byte(0x0F_u8)

          # Message id
          io.write(msg_id.to_slice)

          # Chunk info
          io.write_byte(index.to_u8)
          io.write_byte(num_slices.to_u8)

          # Bytes
          bytes_to_send = [data.size, max_chunk_size].min
          io.write(data[0, bytes_to_send])
          data += bytes_to_send

          @receive_channel.send(io.to_slice)
        end
      end
    end

    private def serialize_message(message)
      io = IO::Memory.new
      deflater = Zlib::Writer.new(io)
      json = message.to_json
      deflater.write(json.to_slice)
      deflater.close
      io.to_slice
    end
  end
end
