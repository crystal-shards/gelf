module GELF
  class Buffer
    property! max_size
    
    def initialize(@max_size = 1000)
      @messages = [] of Slice(UInt8) 
    end

    def write(message : Slice(UInt8))
      @messages.push(message)
      @messages.shift if @messages.size > @max_size 
    end

    def read
      @messages.shift unless size.zero?
    end

    def size
      @messages.size
    end
  end
end