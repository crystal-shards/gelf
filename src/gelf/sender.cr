module GELF
  class Sender(T)
    getter connected, closed, logger
    @socket : T?

    def initialize(@host : String, @port : Int32, @logger : GELF::Logger)
      @connected = false
      @closed = false
      connect
      receive_messages
    end

    def connect
      spawn do
        begin
          @socket = T.new
          socket.connect(@host, @port, connect_timeout: logger.connection_timeout)
          @connected = true
    
          loop do
            message = logger.buffer.read
            break unless message
            write(message)
          end
        rescue e
          logger.disconnect
          logger.disconnect_callback.call(logger, e)
        end
      end
    end

    def socket
      @socket.not_nil!
    end

    def close
      @closed = true
      socket.close
      @connected = false
    end

    private def receive_messages
      spawn do
        loop do
          message = logger.receive_channel.receive
          write(message)
        end
      end
    end

    private def write(message : Slice(UInt8))
      if @socket && @connected
        socket.write(message)
        socket << '\0' if T.is_a?(TCPSocket.class)
      else
        logger.buffer.write(message)
      end
    rescue e
      logger.buffer.write(message)
      logger.disconnect
      logger.disconnect_callback.call(logger, e)
    end
  end
end
