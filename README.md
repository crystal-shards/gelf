# gelf

A GELF compatible logger http://docs.graylog.org/en/latest/pages/gelf.html

## Installation

Add this to your application's `shard.yml`:

```yaml
dependencies:
  gelf:
    git: https://gitlab.com/crystal-shards/gelf
```


## Usage

```crystal
require "gelf"

TestLogger = GELF::Logger.configure do |config|
  config.host     = "graylog.local"
  config.port     = 12204
  config.protocol = "udp"
  config.facility = "crystal-app"
  config.hostname = "localhost"
  config.level    = Logger::DEBUG
  config.disconnect_callback = -> (logger : GELF::Logger, e : Exception) do
    logger.connect

    # puts "logger disconnected, #{e.class} -> #{e.message}"
    # raise e
    # exit 1
  end
end

# simple string
TestLogger.debug("simple message")

# simple hash
TestLogger.info({"short_message" => "simple message", "details" => "simple details"})

# with custom facility
TestLogger.info({"short_message" => "simple message"}, progname: "api-backend")

# with a nested hash (1 lvl only)
TestLogger.error("short_message" => "simple message", "details" => { "x" => 1, "y" => 2 })
```

## Configuration
`* host` - gelf endpoint

`* port` - gelf port

`* facility` - logger facility unless progname sended

`protocol` - tcp or udp (default `tcp`)

`hostname` - application source (default `local`)

`level` - logger severity (default `Logger::INFO`)

`disconnect_callback`  -  callback to be called after disconnect (default `raise`)

`chunk_size` - small  (1420) or big (8154) max message chunk size before split for UDP sending (default `small`)

`buffer_size` - max messages circular buffer capacity when socket disconnected (default `1000`)

`connection_timeout` - max socket connection timeout

## Methods
```crystal
# logger severity
logger.debug
logger.info
logger.warn
logger.error
logger.fatal
logger.unknown

# connection methods
logger.connect
logger.disconnect
logger.connection

# level (property)
logger.level

# disconnect times counter
logger.disconnect_count

# message buffer
logger.buffer
logger.buffer.size
logger.buffer.max_size # property
```

  
  

#### Forked from https://github.com/benoist/gelf-crystal
